package Zoologico;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Login {

    private JFrame frame;
    private JPanel panel;
    private JPanel panel_1;
    private JLabel lblTitulo;
    private JLabel lblUser;
    private JLabel lblPassword;
    private PlaceholderTextField textUser;
    private PlaceholderTextField textPassword;
    private JButton btnLogin;
    private JLabel lblNewLabel;

    public static void main(String[] args) {
        new Login();
    }

    public Login() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.setResizable(false);
        frame.setBounds(100, 100, 400, 550);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setBackground(new Color(223, 221, 208));
        frame.getContentPane().add(panel);
        panel.setLayout(null);

        // Panel Login

        panel_1 = new JPanel();
        panel_1.setBackground(new Color(0, 128, 0));
        panel_1.setBounds(0, 0, 400, 72);
        panel.add(panel_1);
        panel_1.setLayout(null);

        lblTitulo = new JLabel("MI ZOO");
        lblTitulo.setBounds(0, 0, 400, 72);
        lblTitulo.setHorizontalAlignment(JLabel.CENTER);
        panel_1.add(lblTitulo);
        lblTitulo.setForeground(new Color(255, 248, 220));
        lblTitulo.setFont(new Font("Comic Sans MS", Font.PLAIN, 40));

        lblNewLabel = new JLabel("Iniciar Sesi�n");
        lblNewLabel.setForeground(new Color(0, 128, 0));
        lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
        lblNewLabel.setBounds(21, 96, 235, 37);
        panel.add(lblNewLabel);

        lblUser = new JLabel("Usuario");
        lblUser.setForeground(new Color(0, 128, 0));
        lblUser.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblUser.setBounds(38, 160, 117, 31);
        panel.add(lblUser);

        textUser = new PlaceholderTextField("Ingrese Usuario");
        textUser.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (textUser.getText().equals("Ingrese Usuario")) {
                    textUser.resetFields();
                }

                if (textPassword.getText().isEmpty()) {
                    textPassword.resetFields();
                }
            }
        });
        textUser.setFont(new Font("Tahoma", Font.PLAIN, 15));
        textUser.setBounds(92, 217, 201, 31);
        panel.add(textUser);
        textUser.setColumns(10);

        lblPassword = new JLabel("Contrase�a");
        lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 20));
        lblPassword.setForeground(new Color(0, 128, 0));
        lblPassword.setBounds(38, 271, 152, 37);
        panel.add(lblPassword);

        textPassword = new PlaceholderTextField("****");
        textPassword.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (textPassword.getText().equals("****")) {
                    textPassword.resetFields();
                }

                if (textUser.getText().isEmpty()) {
                    textUser.resetFields();
                }
            }
        });
        textPassword.setFont(new Font("Tahoma", Font.PLAIN, 15));
        textPassword.setBounds(92, 333, 201, 31);
        panel.add(textPassword);
        textPassword.setColumns(10);

        btnLogin = new JButton("Log in");
        btnLogin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String enteredUser = textUser.getRealText().toLowerCase();
                String enteredPass = textPassword.getText();

                if ("admin".equals(enteredUser) && "1234".equals(enteredPass)) {
                    frame.setVisible(false);
                    textUser.resetFields();
                    textPassword.resetFields();

                    Admin ventanaA = new Admin(frame);
                    ventanaA.getFrame().setVisible(true);
                } else if ("user".equals(enteredUser) && "1234".equals(enteredPass)) {
                    frame.setVisible(false);
                    textUser.resetFields();
                    textPassword.resetFields();

                    Trabajador ventanaT = new Trabajador(frame);
                    ventanaT.getFrame().setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(frame, "Usuario y/o contrase�a incorrectos");
                }
            }
        });
        
        btnLogin.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        btnLogin.setBackground(new Color(0, 128, 0));
        btnLogin.setForeground(new Color(255, 248, 220));
        btnLogin.setFont(new Font("Tahoma", Font.BOLD, 15));
        btnLogin.setBounds(137, 419, 102, 37);
        panel.add(btnLogin);

        frame.setVisible(true);
    }

	public JFrame getFrame() {

		return frame;
	}
}
    
