package Zoologico;

public enum Habitat {

    HAEREO("A�reo", "Disponible", "Limpio"),
    HAQUA("Acu�tico", "Disponible", "Necesita limpieza"),
    HTERRA("Terrestre", "Disponible", "Limpio"),
    HARBOREO("Arb�reo", "Disponible", "Limpio"),
    HMIX("Mixto", "No disponible", "Necesita limpieza");

    private final String description;
    private final String availability;
    private final String limpio;

    Habitat(String description, String availability, String limpio) {
        this.description = description;
        this.availability = availability;
        this.limpio = limpio;
    }

    public String getDescription() {
        return description;
    }

    public String getAvailability() {
        return availability;
    }

	public String getLimpio() {

		return limpio;
	}
}


