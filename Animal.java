package Zoologico;

import java.util.ArrayList;
import java.util.Date;

public class Animal {
        
        ArrayList<Veterinario> veterinarios;
        ArrayList<Cuidador> cuidadores;
        ArrayList<Comida> comidas;
        Especie especie; //enum
        int edad;
        String genero;
        Date fechaNacimiento;
        String origen;
        Habitat habitat; //enum

}
