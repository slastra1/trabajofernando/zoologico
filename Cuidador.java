package Zoologico;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;

public class Cuidador extends Empleado{

    public Cuidador(String nombre, int edad, boolean genero, String telefono, LocalDate fechaContratacion, double salario,LocalTime horarioTrabajo, String ocupado) {
        super(nombre, edad, genero, telefono, fechaContratacion, salario,horarioTrabajo, ocupado);

    }

    public void alimentarAnimales() {
        // L�gica para alimentar animales
    }
    

    @Override
    public float CalcularSalario() {
        int diasTrabajados = generarDiasTrabajados();
        int horasTrabajadasPorDia = generarHorasTrabajadasPorDia();

        return InterfazEmpleados.salarioVeterinario * diasTrabajados * horasTrabajadasPorDia;
    }

    private int generarDiasTrabajados() {
        // Genera un n�mero aleatorio de d�as trabajados entre 1 y 30 (suponiendo un mes de trabajo)
        return new Random().nextInt(30) + 1;
    }

    private int generarHorasTrabajadasPorDia() {
        // Genera un n�mero aleatorio de horas trabajadas por d�a entre 1 y 8 (suponiendo un d�a de trabajo)
        return new Random().nextInt(8) + 1;
    }
	
	@Override
    public String toString() {
        return super.toString();
    }


}
