package Zoologico;

public enum EspecialidadVeterinaria {
	
	CIRUGIA("Cirugía"),
    CARDIOLOGIA("Cardiología"),
    DERMATOLOGIA("Dermatología"),
    ONCOLOGIA("Oncología"),
    ODONTOLOGIA("Odontología"),
    NUTRICION("Nutrición"),
    NEUROLOGIA("Neurología"),
    OFTALMOLOGIA("Oftalmología");

    private final String nombre;

    EspecialidadVeterinaria(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
}
