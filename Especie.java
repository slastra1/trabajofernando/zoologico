package Zoologico;

public enum Especie {
	
	LEON("Le�n", TipoAnimal.TERRESTRE),
    DELFIN("Delf�n", TipoAnimal.ACUATICO),
    AGUILA("�guila", TipoAnimal.AEREO),
    MONO("Mono", TipoAnimal.ARBOL),
    PINGUINO("Ping�ino", TipoAnimal.ACUATICO),
    JIRAFA("Jirafa", TipoAnimal.TERRESTRE);

    private final String nombre;
    private final TipoAnimal tipo;

    Especie(String nombre, TipoAnimal tipo) {
        this.nombre = nombre;
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public TipoAnimal getTipo() {
        return tipo;
    }
}
