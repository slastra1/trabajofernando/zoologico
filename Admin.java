package Zoologico;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;

public class Admin {

    private JFrame frame;
    private JPanel panel;
    private JPanel panel_1;
    private JLabel lblTitulo;
    private JButton btnCalcularSalario;
    private JButton btnContratarDespedir;
    private JButton btnListarEmpleados;
    private JButton btnVolver;
    private JFrame loginFrame;

    // Asumiendo que tienes las listas de veterinarios y cuidadores previamente definidas y populadas
    List<Veterinario> veterinarios = new ArrayList<>();
    List<Cuidador> cuidadores = new ArrayList<>();

    public Admin(JFrame loginFrame) {
        this.loginFrame = loginFrame;
        initialize();
     // Agregar 3 Veterinarios.
        veterinarios.add(new Veterinario("Luis Quintal", 30, true, "123456789", LocalDate.now(), 2000.0, LocalTime.of(9, 0), EspecialidadVeterinaria.CIRUGIA, "Libre"));
        veterinarios.add(new Veterinario("Maria de la O", 35, false, "987654321", LocalDate.now(), 2200.0, LocalTime.of(10, 0), EspecialidadVeterinaria.CARDIOLOGIA, "Ocupado"));
        veterinarios.add(new Veterinario("Juanita Valderrama", 28, true, "555555555", LocalDate.now(), 1800.0, LocalTime.of(11, 0), EspecialidadVeterinaria.NUTRICION, "Ocupado"));

        // Agregar 3 Cuidadores.
        cuidadores.add(new Cuidador("Scanlan Shorthalt", 25, false, "111111111", LocalDate.now(), 1500.0, LocalTime.of(8, 0), "Libre"));
        cuidadores.add(new Cuidador("Grog Strongjaw", 40, true, "222222222", LocalDate.now(), 1700.0, LocalTime.of(12, 0), "Ocupado"));
        cuidadores.add(new Cuidador("Pike Trickfoot", 32, false, "333333333", LocalDate.now(), 1600.0, LocalTime.of(14, 0), "Libre"));
    }

    private void initialize() {
        frame = new JFrame();
        frame.setResizable(false);
        frame.setBounds(100, 100, 400, 550);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel();
        panel.setBackground(new Color(223, 221, 208));
        frame.getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(null);

        panel_1 = new JPanel();
        panel_1.setBackground(new Color(0, 128, 0));
        panel_1.setBounds(0, 0, 400, 72);
        panel.add(panel_1);

        lblTitulo = new JLabel("MI ZOO");
        lblTitulo.setForeground(new Color(255, 248, 220));
        lblTitulo.setFont(new Font("Comic Sans MS", Font.PLAIN, 40));
        panel_1.add(lblTitulo);

        // Bot�n Calcular Salario.
        btnCalcularSalario = new JButton("Calcular salario");
        
        //Logica Calcuar Salario.
        btnCalcularSalario.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	// Crear un StringBuilder para construir el mensaje.
                StringBuilder mensaje = new StringBuilder();

                // Calcular el salario para cada cuidador
                mensaje.append("Cuidadores:\n");
                for (Cuidador cuidador : cuidadores) {
                    mensaje.append("Nombre: ").append(cuidador.getNombre()).append("\n");
                    mensaje.append("Salario: $").append(cuidador.CalcularSalario()).append("\n");
                    mensaje.append("\n");
                }

                // A�adir separador
                mensaje.append("\n");

                // Calcular el salario para cada veterinario
                mensaje.append("Veterinarios:\n");
                for (Veterinario veterinario : veterinarios) {
                    mensaje.append("Nombre: ").append(veterinario.getNombre()).append("\n");
                    mensaje.append("Salario: $").append(veterinario.CalcularSalario()).append("\n");
                    mensaje.append("\n");
                }

                // Mostrar el cuadro de di�logo con los detalles de los salarios
                JOptionPane.showMessageDialog(frame, mensaje.toString());
            }
        });
        
        //Configruaci�n Calcular Salario
        btnCalcularSalario.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        btnCalcularSalario.setBackground(new Color(0, 128, 0));
        btnCalcularSalario.setIcon(new ImageIcon("icons/calculadora.png"));
        btnCalcularSalario.setForeground(new Color(255, 250, 240));
        btnCalcularSalario.setFont(new Font("Tahoma", Font.BOLD, 15));
        btnCalcularSalario.setBounds(59, 108, 263, 72);
        panel.add(btnCalcularSalario);

        // Bot�n Contratar/Despedir.
        btnContratarDespedir = new JButton("Contratar / Despedir");
        
        btnContratarDespedir.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Crear un JOptionPane con opciones Contratar o Despedir
                Object[] options = {"Contratar", "Despedir"};
                int mainChoice = JOptionPane.showOptionDialog(frame, "Seleccione una opci�n:", "Contratar/Despedir",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

                if (mainChoice == JOptionPane.YES_OPTION) {
                    // Opci�n Contratar: Mostrar JOptionPane para elegir entre Cuidador y Veterinario
                    Object[] employeeOptions = {"Cuidador", "Veterinario"};
                    int employeeTypeChoice = JOptionPane.showOptionDialog(frame, "Seleccione el tipo de empleado:",
                            "Contratar", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, employeeOptions,
                            employeeOptions[0]);

                    if (employeeTypeChoice == JOptionPane.YES_OPTION) {
                    	
                        // Contratar Cuidador
                    	String nombre = JOptionPane.showInputDialog(frame, "Ingrese el nombre del nuevo empleado:");
                    	int edad = Integer.parseInt(JOptionPane.showInputDialog(frame, "Ingrese la edad del nuevo empleado:"));
                    	boolean genero = JOptionPane.showInputDialog(frame, "Ingrese el g�nero del nuevo empleado (Masculino/Femenino):")
                    	        .equalsIgnoreCase("Masculino");
                    	String telefono = JOptionPane.showInputDialog(frame, "Ingrese el tel�fono del nuevo empleado:");
                    	double salario = Double.parseDouble(JOptionPane.showInputDialog(frame, "Ingrese el salario del nuevo empleado:"));
                    	LocalDate fechaContratacion = LocalDate.now(); // Fecha actual.
                    	LocalTime horarioTrabajo = LocalTime.of(8, 0); // Horario inicial.
                    	String ocupado = "Libre"; // Estado inicial.

                        // Agregar el nuevo cuidador a la lista de cuidadores
                        Cuidador nuevoCuidador = new Cuidador(nombre, edad, genero, telefono, LocalDate.now(), salario,
                                LocalTime.of(8, 0), "Libre");
                        cuidadores.add(nuevoCuidador);
                        JOptionPane.showMessageDialog(frame, "Cuidador contratado con �xito.");
                    } else if (employeeTypeChoice == JOptionPane.NO_OPTION) {
                    	
                        // Contratar Veterinario
                    	String nombre = JOptionPane.showInputDialog(frame, "Ingrese el nombre del nuevo empleado:");
                    	int edad = Integer.parseInt(JOptionPane.showInputDialog(frame, "Ingrese la edad del nuevo empleado:"));
                    	boolean genero = JOptionPane.showInputDialog(frame, "Ingrese el g�nero del nuevo empleado (Masculino/Femenino):")
                    	        .equalsIgnoreCase("Masculino");
                    	String telefono = JOptionPane.showInputDialog(frame, "Ingrese el tel�fono del nuevo empleado:");
                    	double salario = Double.parseDouble(JOptionPane.showInputDialog(frame, "Ingrese el salario del nuevo empleado:"));
                    	LocalDate fechaContratacion = LocalDate.now(); // Fecha actual.
                    	LocalTime horarioTrabajo = LocalTime.of(8, 0); 
                    	String ocupado = "Libre"; // Estado inicial.
                    	// Crear un JComboBox para seleccionar la especialidad
                        JComboBox<EspecialidadVeterinaria> especialidadComboBox = new JComboBox<>(EspecialidadVeterinaria.values());
                        JOptionPane.showMessageDialog(frame, especialidadComboBox, "Seleccione la especialidad del nuevo veterinario:",
                                JOptionPane.QUESTION_MESSAGE);

                        // Obtener la especialidad seleccionada
                        EspecialidadVeterinaria especialidad = (EspecialidadVeterinaria) especialidadComboBox.getSelectedItem();

                        // Agregar el nuevo veterinario a la lista de veterinarios
                        Veterinario nuevoVeterinario = new Veterinario(nombre, edad, genero, telefono, LocalDate.now(), salario,
                                LocalTime.of(0, 0), especialidad, "Libre");
                        veterinarios.add(nuevoVeterinario);
                        JOptionPane.showMessageDialog(frame, "Veterinario contratado con �xito.");
                    }
                } else if (mainChoice == JOptionPane.NO_OPTION) {
                    // Opci�n Despedir: Mostrar JOptionPane para ingresar el nombre del empleado a despedir
                    String nombreEmpleado = JOptionPane.showInputDialog(frame, "Ingrese el nombre del empleado a despedir:");

                    // Buscar y despedir al empleado si existe
                    boolean empleadoEncontrado = false;
                    for (Veterinario veterinario : veterinarios) {
                        if (veterinario.getNombre().equalsIgnoreCase(nombreEmpleado)) {
                            veterinarios.remove(veterinario);
                            empleadoEncontrado = true;
                            break;
                        }
                    }

                    for (Cuidador cuidador : cuidadores) {
                        if (cuidador.getNombre().equalsIgnoreCase(nombreEmpleado)) {
                            cuidadores.remove(cuidador);
                            empleadoEncontrado = true;
                            break;
                        }
                    }

                    // Mostrar el resultado en un JOptionPane
                    if (empleadoEncontrado) {
                        JOptionPane.showMessageDialog(frame, "Empleado despedido con �xito.");
                    } else {
                        JOptionPane.showMessageDialog(frame,
                                "El empleado no existe. Por favor, pruebe de nuevo.");
                    }
                }
            }
        });

        
        //Configuraci�n Contratar/Despedir.
        btnContratarDespedir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        btnContratarDespedir.setIcon(new ImageIcon("icons/sobre.png"));
        btnContratarDespedir.setForeground(new Color(255, 250, 240));
        btnContratarDespedir.setFont(new Font("Tahoma", Font.BOLD, 15));
        btnContratarDespedir.setBackground(new Color(0, 128, 0));
        btnContratarDespedir.setBounds(59, 216, 263, 72);
        panel.add(btnContratarDespedir);
        
      //Nombre del boton listar empleados
        btnListarEmpleados = new JButton("Listar empleados");
      //l�gica listar empleados
        btnListarEmpleados.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	// Crear un StringBuilder para construir el mensaje.
            	StringBuilder mensaje = new StringBuilder();
            		

   		  // Listar Cuidadores
   		     mensaje.append("Cuidadores:\n");
   		     for (Cuidador cuidador : cuidadores) {
   		         mensaje.append("Nombre: ").append(cuidador.getNombre()).append("\n");
   		         mensaje.append("Edad: ").append(cuidador.getEdad()).append(" -- ");
   		         mensaje.append("Genero: ").append(cuidador.isGenero() ? "Masculino" : "Femenino").append(" -- ");
   		         mensaje.append("Tel�fono: ").append(cuidador.getTelefono()).append(" -- ");
   		         mensaje.append("Fecha Contrataci�n: ").append(cuidador.getFechaContratacion()).append(" -- ");
   		         mensaje.append("Salario: ").append(cuidador.getSalario()).append(" -- ");
   		         mensaje.append("Horario de Trabajo: ").append(cuidador.getHorarioTrabajo()).append("\n");
   		         mensaje.append("Ocupado: ").append(cuidador.getOcupado()).append("\n");
   		         mensaje.append("\n");
   		     }

   		     // A�adir separador
   		     mensaje.append("\n");

   		     // Listar Veterinarios
   		     mensaje.append("Veterinarios:\n");
   		     for (Veterinario veterinario : veterinarios) {
   		         mensaje.append("Nombre: ").append(veterinario.getNombre()).append("\n");
   		         mensaje.append("Edad: ").append(veterinario.getEdad()).append(" -- ");
   		         mensaje.append("Genero: ").append(veterinario.isGenero() ? "Masculino" : "Femenino").append(" -- ");
   		         mensaje.append("Tel�fono: ").append(veterinario.getTelefono()).append(" -- ");
   		         mensaje.append("Fecha Contrataci�n: ").append(veterinario.getFechaContratacion()).append(" -- ");
   		         mensaje.append("Salario: ").append(veterinario.getSalario()).append(" -- ");
   		         mensaje.append("Horario Trabajo: ").append(veterinario.getHorarioTrabajo()).append("\n");
   		         mensaje.append("Especialidad: ").append(veterinario.getEspecialidad()).append("\n");
   		         mensaje.append("Ocupado: ").append(veterinario.getOcupado()).append("\n");
   		         mensaje.append("\n");
   		     }

   		     // Mostrar el cuadro de di�logo con los detalles de los empleados
   		     JOptionPane.showMessageDialog(frame, mensaje.toString());
   		 }
            });
        
        //Configuraci�n del bot�n para gestionar listar empleados.
        btnListarEmpleados.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        btnListarEmpleados.setIcon(new ImageIcon("icons/identificacion.png"));
        btnListarEmpleados.setForeground(new Color(255, 250, 240));
        btnListarEmpleados.setFont(new Font("Tahoma", Font.BOLD, 15));
        btnListarEmpleados.setBackground(new Color(0, 128, 0));
        btnListarEmpleados.setBounds(59, 326, 263, 72);
        panel.add(btnListarEmpleados);

        btnVolver = new JButton("Volver");
        btnVolver.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                loginFrame.setVisible(true);
            }
        });
        btnVolver.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        btnVolver.setForeground(new Color(255, 255, 255));
        btnVolver.setFont(new Font("Tahoma", Font.BOLD, 10));
        btnVolver.setBackground(new Color(255, 0, 0));
        btnVolver.setBounds(150, 445, 87, 29);
        panel.add(btnVolver);
    }

    public JFrame getFrame() {
        return frame;
    }
}

