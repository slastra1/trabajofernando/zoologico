package Zoologico;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public abstract class Empleado implements InterfazEmpleados {

    private String nombre;
    private int edad;
    private boolean genero;
    private String telefono;
    private LocalDate fechaContratacion;
    private double salario;
    private LocalTime horarioTrabajo;
    private String ocupado;
    private ArrayList<Animal> animal;

    public Empleado() {
        this.nombre = " ";
        this.edad = 0;
        this.genero = false;
        this.telefono = "";
        this.fechaContratacion = LocalDate.now();
        this.salario = 0.0;
        this.horarioTrabajo = LocalTime.of(0, 0);
        this.ocupado = "";
    }

    public Empleado(String nombre, int edad, boolean genero, String telefono, LocalDate fechaContratacion,
            double salario, LocalTime horarioTrabajo, String ocupado) {
        super();
        this.nombre = nombre;
        this.edad = edad;
        this.genero = genero;
        this.telefono = telefono;
        this.fechaContratacion = fechaContratacion;
        this.salario = salario;
        this.horarioTrabajo = horarioTrabajo;
        this.ocupado = ocupado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public boolean isGenero() {
        return genero;
    }

    public void setGenero(boolean genero) {
        this.genero = genero;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public LocalDate getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(LocalDate fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public LocalTime getHorarioTrabajo() {
        return horarioTrabajo;
    }

    public void setHorarioTrabajo(LocalTime horarioTrabajo) {
        this.horarioTrabajo = horarioTrabajo;
    }

    public String getOcupado() {
        return this.ocupado;
    }
    
    public void setOcupado(String ocupado) {
        this.ocupado = ocupado;
    }

    @Override
    public String toString() {
        return "Empleado [nombre=" + getNombre() + ", edad=" + getEdad() + ", genero=" + isGenero() + ", telefono=" + getTelefono()
                + ", fechaContratacion=" + getFechaContratacion() + ", salario=" + getSalario() + ", horarioTrabajo="
                + getHorarioTrabajo() + ", ocupación=" + getOcupado() + "]";
    }


}
