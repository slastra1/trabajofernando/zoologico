package Zoologico;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import javax.swing.JTextField;

public class PlaceholderTextField extends JTextField {
    private String placeholder;
    private String realText;

    public PlaceholderTextField(String placeholder) {
        super();
        this.placeholder = placeholder;
        setupPlaceholder();

        addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                if (getText().equals(placeholder)) {
                    setText("");
                    setForeground(Color.black);
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (getText().isEmpty()) {
                    setupPlaceholder(); // Restaurar el placeholder si el campo est� vac�o
                } else {
                    realText = getText(); // Actualiza el valor real cuando se pierde el foco
                }
            }
        });
    }

    private void setupPlaceholder() {
        setForeground(new Color(192, 192, 192));
        setText(placeholder);
        realText = ""; // Establece el valor real como cadena vac�a al configurar el marcador de posici�n
    }
    
    public String getRealText() {
        return realText;
    }

    public void resetFields() {
        setText(placeholder);
        setForeground(new Color(192, 192, 192));
        realText = "";
    }
}
