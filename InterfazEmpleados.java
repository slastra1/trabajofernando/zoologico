package Zoologico;

public interface InterfazEmpleados {

	static final float salarioVeterinario = 9.02f;
    static final float salarioCuidador = 9.02f;
    static final float peligrosidad = 3.50f;

     float CalcularSalario();
    
}
