package Zoologico;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Comida {
	
	//Atributos
	String nombre;
	String tipo;
	LocalDate fechaVencimiento;
	Double cantidadDisponible;
	String nutrientes;
	String paisOrigen;
	 
    //Constructores
    public Comida() {
        this.nombre = "Sin definir";
        this.tipo = "Sin definir";
        this.fechaVencimiento = LocalDate.MIN; // O puedes asignar null
        this.cantidadDisponible = 0.0;
        this.nutrientes = "Sin definir";
        this.paisOrigen = "Sin definir";
    }
	
	public Comida(String nombre, String tipo, LocalDate fechaVencimiento, Double cantidadDisponible, String nutrientes, String paisOrigen) {
	    this.nombre = nombre;
	    this.tipo = tipo;
	    this.fechaVencimiento = fechaVencimiento;
	    this.cantidadDisponible = cantidadDisponible;
	    this.nutrientes = nutrientes;
	    this.paisOrigen = paisOrigen;
	}


	//Getter and Setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public LocalDate getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(LocalDate fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public Double getCantidadDisponible() {
		return cantidadDisponible;
	}

	public void setCantidadDisponible(Double cantidadDisponible) {
		this.cantidadDisponible = cantidadDisponible;
	}

	public String getNutrientes() {
		return nutrientes;
	}

	public void setNutrientes(String nutrientes) {
		this.nutrientes = nutrientes;
	}

	public String getPaisOrigen() {
		return paisOrigen;
	}

	public void setPaisOrigen(String paisOrigen) {
		this.paisOrigen = paisOrigen;
	}
	
	//Metodo toString
	public String toString() {
        DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd-MM-yy");
        String fechaFormateada = (fechaVencimiento != null) ? fechaVencimiento.format(formato) : "Sin fecha";
        return "Comida {" +
                "nombre='" + nombre + '\'' +
                ", tipo='" + tipo + '\'' +
                ", fechaVencimiento=" + fechaFormateada +
                ", cantidadDisponible=" + cantidadDisponible +
                ", nutrientes='" + nutrientes + '\'' +
                ", paisOrigen='" + paisOrigen + '\'' +
                '}';
    }
	
}
