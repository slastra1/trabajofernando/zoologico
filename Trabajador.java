package Zoologico;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Cursor;

public class Trabajador {

	private JFrame frame;
	private JPanel panel;
	private JPanel panel_1;
	private JLabel lblTitulo;
	private JButton btnListarAnimales;
	private JButton btnStockComida;
	private JButton btnEstadoHabitats;
	private JButton btnEstadoDisponibilidad;
	private JButton btnVolver;
	private List<Comida> stockComida;
	
	// Asumiendo que tienes las listas de veterinarios y cuidadores previamente definidas y populadas
    List<Veterinario> veterinarios = new ArrayList<>();
    List<Cuidador> cuidadores = new ArrayList<>();

	/**
	 * Create the application.
	 */

	public Trabajador(JFrame loginFrame) {
	    initialize(loginFrame);
	 // Agregar 3 Veterinarios.
        veterinarios.add(new Veterinario("Luis Quintal", 30, true, "123456789", LocalDate.now(), 2000.0, LocalTime.of(9, 0), EspecialidadVeterinaria.CIRUGIA, "Libre"));
        veterinarios.add(new Veterinario("Maria de la O", 35, false, "987654321", LocalDate.now(), 2200.0, LocalTime.of(10, 0), EspecialidadVeterinaria.CARDIOLOGIA, "Ocupado"));
        veterinarios.add(new Veterinario("Juanita Valderrama", 28, true, "555555555", LocalDate.now(), 1800.0, LocalTime.of(11, 0), EspecialidadVeterinaria.NUTRICION, "Ocupado"));

        // Agregar 3 Cuidadores.
        cuidadores.add(new Cuidador("Scanlan Shorthalt", 25, false, "111111111", LocalDate.now(), 1500.0, LocalTime.of(8, 0), "Libre"));
        cuidadores.add(new Cuidador("Grog Strongjaw", 40, true, "222222222", LocalDate.now(), 1700.0, LocalTime.of(12, 0), "Ocupado"));
        cuidadores.add(new Cuidador("Pike Trickfoot", 32, false, "333333333", LocalDate.now(), 1600.0, LocalTime.of(14, 0), "Libre"));
	    
	    try {
	        // Inicializar el stock de comida
	        initStockComida();
	    } catch (Exception e) {
	        e.printStackTrace();
	        JOptionPane.showMessageDialog(frame, "Error al inicializar el stock de comida: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	    }
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(JFrame Login) {
		frame = new JFrame();
		frame.setBounds(100, 100, 400, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel();
		panel.setBackground(new Color(223, 221, 208));
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 128, 0));
		panel_1.setBounds(0, 0, 400, 72);
		panel.add(panel_1);
		
		lblTitulo = new JLabel("MI ZOO");
		lblTitulo.setForeground(new Color(255, 248, 220));
		lblTitulo.setFont(new Font("Comic Sans MS", Font.PLAIN, 40));
		panel_1.add(lblTitulo);
		
		//Nombre del boton.
		btnListarAnimales = new JButton("Listar animales");
		
		//Logica de Listar.
		btnListarAnimales.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        // Obtener los valores del enum Especie.
		        Especie[] especies = Especie.values();

		        // Crear un StringBuilder para construir el mensaje.
		        StringBuilder mensaje = new StringBuilder("Lista de animales:\n");

		        // Iterar sobre los valores del enum y agregar sus nombres y tipos al mensaje.
		        for (Especie especie : especies) {
		            mensaje.append(especie.getNombre()).append(" - ").append(especie.getTipo()).append("\n");
		        }

		        // Mostrar el cuadro de di�logo con la lista de animales.
		        JOptionPane.showMessageDialog(frame, mensaje.toString());
		    }
		});
		
		// Configuraci�n del bot�n para gestionar listar animales.
		btnListarAnimales.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnListarAnimales.setBackground(new Color(0, 128, 0));
		btnListarAnimales.setIcon(new ImageIcon("icons/informacion.png"));
		btnListarAnimales.setForeground(new Color(255, 250, 240));
		btnListarAnimales.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnListarAnimales.setBounds(52, 94, 278, 72);
		panel.add(btnListarAnimales);
		
		//Nombre del boton Stock comida.
		btnStockComida = new JButton("Stock de comida");
		
		//Logica del Stock comida.
		btnStockComida.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        // Obtener el stock de comida
		        List<Comida> stockComida = getStockComida();

		        // Crear un StringBuilder para construir el mensaje
		        StringBuilder mensaje = new StringBuilder("Stock de comida:\n");

		        // Iterar sobre las comidas y agregar sus detalles al mensaje
		        for (Comida comida : stockComida) {
		            mensaje.append(comida.toString()).append("\n");
		        }

		        // Mostrar el cuadro de di�logo con el stock de comida
		        JOptionPane.showMessageDialog(frame, mensaje.toString());
		    }
		});
		
		// Configuraci�n del bot�n para gestionar el stock de comida.
		btnStockComida.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnStockComida.setIcon(new ImageIcon("icons/comida.png"));
		btnStockComida.setForeground(new Color(255, 250, 240));
		btnStockComida.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnStockComida.setBackground(new Color(0, 128, 0));
		btnStockComida.setBounds(52, 183, 278, 72);
		panel.add(btnStockComida);
		
		//Nombre de boton Estado habitats.
		btnEstadoHabitats = new JButton("Estado de los habitats");
		
		//Logica estado habitats
		btnEstadoHabitats.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        // Crear un StringBuilder para construir el mensaje
		        StringBuilder mensaje = new StringBuilder("Estado de los habitats:\n");

		        // Iterar sobre los valores del enum Habitat y agregar sus detalles al mensaje
		        for (Habitat habitat : Habitat.values()) {
		            mensaje.append(habitat.getDescription()).append(" - ").append(habitat.getAvailability()).append(" -> ").append(habitat.getLimpio()).append("\n");
		        }

		        // Mostrar el cuadro de di�logo con el estado de los habitats
		        JOptionPane.showMessageDialog(frame, mensaje.toString());
		    }
		});
		
		// Configuraci�n del bot�n para gestionar el Estado de habitats.
		btnEstadoHabitats.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnEstadoHabitats.setIcon(new ImageIcon("icons/habitats.png"));
		btnEstadoHabitats.setForeground(new Color(255, 250, 240));
		btnEstadoHabitats.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnEstadoHabitats.setBackground(new Color(0, 128, 0));
		btnEstadoHabitats.setBounds(52, 275, 278, 72);
		panel.add(btnEstadoHabitats);
		
		//Nombre de boton Disponibilidad.
		btnEstadoDisponibilidad = new JButton("Estado de disponibilidad");
		
		//Logica de Disponibilidad.
		btnEstadoDisponibilidad.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        // Crear un StringBuilder para construir el mensaje.
		        StringBuilder mensaje = new StringBuilder();

		        // Listar Cuidadores.
                mensaje.append("Cuidadores:\n");
                for (Cuidador cuidador : cuidadores) {
                    mensaje.append(cuidador.getNombre()).append(" - ").append(cuidador.getOcupado()).append("\n");
                }

                // A�adir separador.
                mensaje.append("\n");

                // Listar Veterinarios.
                mensaje.append("Veterinarios:\n");
                for (Veterinario veterinario : veterinarios) {
                    mensaje.append(veterinario.getNombre()).append(" - ").append(veterinario.getOcupado()).append("\n");
                }

                // Mostrar el cuadro de di�logo con el estado disponible.
		        JOptionPane.showMessageDialog(frame, mensaje.toString());
            }
        });
		
		// Configuraci�n del bot�n para gestionar la Disponibildiad.
		btnEstadoDisponibilidad.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnEstadoDisponibilidad.setIcon(new ImageIcon("icons/estado.png"));
		btnEstadoDisponibilidad.setForeground(new Color(255, 250, 240));
		btnEstadoDisponibilidad.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnEstadoDisponibilidad.setBackground(new Color(0, 128, 0));
		btnEstadoDisponibilidad.setBounds(52, 366, 278, 72);
		panel.add(btnEstadoDisponibilidad);
		
		//Nombre del boton Volver.
		btnVolver = new JButton("Volver");
		
		//logica de Volver.
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				Login.setVisible(true);
			}
		});
		
		// Configuraci�n del bot�n Vovler.
		btnVolver.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnVolver.setFont(new Font("Tahoma", Font.BOLD, 10));
		btnVolver.setForeground(new Color(255, 255, 255));
		btnVolver.setBackground(new Color(255, 0, 0));
		btnVolver.setBounds(150, 466, 87, 29);
		panel.add(btnVolver);
		
	}
	
	//Almacenamos comida, ya que, los trabajadores se encargan de eso.
	private void initStockComida() {
        stockComida = new ArrayList<>();

        // Agregar comidas por defecto.
        Comida comida1 = new Comida("Alimento para leones", "Carne", LocalDate.now().plusDays(30), 100.0, "Rico en prote�nas", "Argentina");
        Comida comida2 = new Comida("Pescado para delfines", "Pescado", LocalDate.now().plusDays(45), 150.0, "Fuente de omega-3", "Chile");

        // Agregar las comidas a la lista de stock.
        stockComida.add(comida1);
        stockComida.add(comida2);
    }
	
	// M�todo para obtener el stock de comida
    public List<Comida> getStockComida() {
        return stockComida;
    }
    
	public JFrame getFrame() {
		return frame;
	}
}
